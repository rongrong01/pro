package _03objects.P8_9;

/**
 * Created by Haining on 10/16/16.
 */
public class ComboLock {

    private int secret1;
    private int secret2;
    private int secret3;
    private int dial;
    private final int MAX_NUMBERS = 40; //dial can only in range [0,39]
    private int stage; //current stage of unlocking

    public ComboLock(int secret1, int secret2, int secret3){
        this.secret1 = secret1;
        this.secret2 = secret2;
        this.secret3 = secret3;
        this.dial = 0;
        this.stage = 1;
    }

    public void reset() {
        dial = 0;
        stage = 1;
    }

    public void turnLeft(int ticks){
        dial = (dial + ticks) % MAX_NUMBERS;
    }

    public void turnRight(int ticks) {
        dial = (dial + (MAX_NUMBERS - ticks % MAX_NUMBERS)) % MAX_NUMBERS;
    }


    public boolean open(){
        if (stage == 1){
            if (dial == secret1){
                stage++;
                System.out.println("First secret unlocked");
                return true;
            }else{
                System.out.println("First secret failed");
            }
        }else if(stage==2){
            if(dial == secret2){
                stage++;
                System.out.println("Second secret unloced");
                return true;
            }else{
                System.out.println("Second secret failed");
            }
        }else if(stage==3){
            if(dial == secret3){
                stage++;
                System.out.println("Third secret unloced");
                return true;
            }else{
                System.out.println("Third secret failed");
            }
        }
        return false;
    }

}
