package _03objects.P8_9;

/**
 * Created by Haining on 10/16/16.
 */
public class Driver {
    public static void main(String[] args) {
        ComboLock lock1 = new ComboLock(39,25,35);
        System.out.println("Lock: 39,25,35");
        System.out.println();
        lock1.turnRight(1);
        lock1.open();
        lock1.turnLeft(26);
        lock1.open();
        lock1.turnRight(30);
        lock1.open();
    }
}
