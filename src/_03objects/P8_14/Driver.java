package _03objects.P8_14;

import java.util.Scanner;

/**
 * Created by Haining on 10/16/16.
 */
public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter radius: ");
        double radius = in.nextDouble();
        System.out.print("Please enter height: ");
        double height = in.nextDouble();
        in.close();

        double sphereVolume = Geometry.sphereVolume(radius);
        double sphereSurface = Geometry.sphereSurface(radius);
        double cylinderVolume = Geometry.cylinderVolume(radius,height);
        double cylinderSurface = Geometry.cylinderSurface(radius,height);
        double coneVolume = Geometry.coneVolume(radius,height);
        double coneSurface = Geometry.coneVolume(radius,height);

        System.out.printf("Sphere Volume is: %.2f\n", sphereVolume);
        System.out.printf("Sphere Surface is: %.2f\n", sphereSurface);
        System.out.printf("Cylinder Volume is: %.2f\n", cylinderVolume);
        System.out.printf("Cylinder Surface is: %.2f\n", cylinderSurface);
        System.out.printf("Cone Volume is: %.2f\n", coneVolume);
        System.out.printf("Cone Surface is: %.2f\n", coneSurface);

    }
}
