package _03objects.P8_6;

/**
 * Created by Haining on 10/16/16.
 */
public class Car {
    private final int INITIAL_FUEL = 0;
    private double amountOfFuel;
    private double fuelEfficiency;

    public Car(double fuelEfficiency) {
        this.fuelEfficiency = fuelEfficiency;
        this.amountOfFuel = this.INITIAL_FUEL;
    }

    public double getFuelEfficiency(){
        return this.fuelEfficiency;
    }

    public void addGas(double gas) {
        this.amountOfFuel += gas;
    }

    public void drive(double distance) {
        this.amountOfFuel -= distance / this.fuelEfficiency;
    }

    public double getGasLevel() {
        return this.amountOfFuel;
    }

}
