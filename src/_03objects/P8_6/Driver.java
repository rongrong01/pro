package _03objects.P8_6;

/**
 * Created by Haining on 10/16/16.
 */
public class Driver {
    public static void main(String[] args) {
        //test mycar with fuel efficiency 50 miles per gallon
        Car mycar = new Car(50);

        System.out.println("Car fuel efficiency: " + mycar.getFuelEfficiency());
        System.out.println("Initial fuel lgevel: " + mycar.getGasLevel());
        System.out.println();

        System.out.println("Tank up 20 gallons");
        mycar.addGas(20);
        System.out.println("Current fuel level: " + mycar.getGasLevel());
        System.out.println();

        System.out.println("Drive 100 miles");
        mycar.drive(100);
        System.out.println("Remaining fuel: " + mycar.getGasLevel());
    }
}
