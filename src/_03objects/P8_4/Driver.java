package _03objects.P8_4;

/**
 * Created by Haining on 10/16/16.
 */
public class Driver {
    public static void main(String[] args) {
        //address 1 to test
        Address address1 = new Address(5701, "N Sheridan Rd", "11E", "Chicago", "IL", 60660);
        //address 2 to test
        Address address2 = new Address(1100, "E 58th Street", "Chicago", "IL", 60637);

        System.out.println("Address 1: ");
        System.out.println(address1.print1());
        System.out.println();

        System.out.println("Address 2: ");
        System.out.println(address2.print2());
        System.out.println();

        System.out.print("Address 1 is before address 2: ");
        System.out.println(address1.comesBefore(address2));
    }
}
