package _03objects.P8_4;

/**
 * Created by Haining on 10/16/16.
 */
public class Address {

    private int houseNumber;
    private String streetName;
    private String aptNumber;
    private String city;
    private String state;
    private int postalCode;

    //constructor with apt number
    public Address(int houseNumber,String streetName,String aptNumber,String city,String state, int postalCode){
        this.houseNumber = houseNumber;
        this.streetName = streetName;
        this.aptNumber = aptNumber;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
    }

    //constructor without apt number
    public Address(int houseNumber,String streetName,String city,String state, int postalCode){
        this.houseNumber = houseNumber;
        this.streetName = streetName;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
    }

    //print address with apt number
    public String print1() {
            return String.format("%s %s %s\n%s %s %d", houseNumber, streetName, aptNumber, city, state, postalCode);
    }

    //print address without apt number
    public String print2() {
            return String.format("%s %s\n%s %s %d", houseNumber, streetName, city, state, postalCode);
    }

    //compare adresses
    public boolean comesBefore(Address other){
        return this.postalCode < other.postalCode;
    }


}
