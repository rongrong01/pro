package _03objects.P8_5;

/**
 * Created by Haining on 10/16/16.
 */
public class SodaCan {
    private double height;
    private double radius;

    public SodaCan(double height,double radius){
        this.height = height;
        this.radius = radius;
    }

    public double getHeight(){
        return this.height;
    }

    public double getRadius(){
        return this.radius;
    }
    public double getSurfaceArea(){
        return 2 * Math.PI * this.radius * (this.radius + this.height);
    }

    public double getVolume(){
        return Math.PI * this.radius * this.radius  * this.height;
    }
}
