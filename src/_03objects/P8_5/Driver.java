package _03objects.P8_5;

/**
 * Created by Haining on 10/16/16.
 */
public class Driver {
    public static void main(String[] args) {
        //test soda can with height 3 and radius 2
        SodaCan sodaCan = new SodaCan(3,2);

        System.out.println("height of SodaCan: "+sodaCan.getHeight());
        System.out.println("radius of SodaCan: "+sodaCan.getRadius());
        System.out.println("calculate surface area: "+sodaCan.getSurfaceArea());
        System.out.println("calculate volume: "+sodaCan.getVolume());
    }
}
