package _03objects.P8_8;

/**
 * Created by Haining on 10/16/16.
 */
public class Driver {
    public static void main(String[] args) {
        Student mystudent = new Student("Alice");
        System.out.println("Student name: " + mystudent.getName());

        Grade grade1 = new Grade("A+");
        Grade grade2 = new Grade("B+");
        Grade grade3 = new Grade("B");
        Grade grade4 = new Grade("F-");

        mystudent.addGrade(grade1);
        mystudent.addGrade(grade2);
        mystudent.addGrade(grade3);
        mystudent.addGrade(grade4);

        System.out.printf("Current GPA: %.2f\n", mystudent.getAverageGrade());
    }
}
