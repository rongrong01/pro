package _03objects.P8_8;

/**
 * Created by Haining on 10/16/16.
 */
public class Student {
    private String name;
    private double totalScore;
    private int numberOfScore;
    private final double INITIAL_TOTALSCORE = 0;
    private final int INITIAL_NUMBEROFSCORE = 0;

    public Student(String name){
        this.name = name;
        this.totalScore = INITIAL_TOTALSCORE;
        this.numberOfScore = INITIAL_NUMBEROFSCORE;
    }

    public String getName(){
        return this.name;
    }

    //add a grade
    public void addGrade(Grade grade) {
        this.numberOfScore++;
        this.totalScore += grade.getScore();
    }

    //get current GPA
    public double getAverageGrade() {
        return this.totalScore / this.numberOfScore;
    }
}
