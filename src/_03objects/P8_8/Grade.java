package _03objects.P8_8;

/**
 * Created by Haining on 10/16/16.
 */
public class Grade {
    private String grade;
    private double score;
    private final int INITIAL_SCORE = 0;

    public Grade(String grade) {
        this.grade = grade;
        this.score = INITIAL_SCORE;
    }

    public double gradeToScore() {
        char gradeLetter = this.grade.charAt(0);
        char gradeSign = ' ';

        if (this.grade.length() == 2) {
            gradeSign = this.grade.charAt(1);
        }


        if (gradeLetter == 'A') {
            score = 4;
        } else if (gradeLetter == 'B') {
            score = 3;
        } else if (gradeLetter == 'C') {
            score = 2;
        } else if (gradeLetter == 'D') {
            score = 1;
        } else if (gradeLetter == 'F') {
            score = 0;
        }

        if (gradeLetter != 'A' && gradeSign == '+') {  // A+ = 4.0
            score += 0.3;
        } else if (gradeLetter != 'F' && gradeSign == '-') { // F- = 0
            score -= 0.3;
        }

     return score;
}

    public double getScore() {
        return this.gradeToScore();
    }

}
