package _03objects.P8_11;

/**
 * Created by Haining on 10/16/16.
 */
public class Letter {
    private String from;
    private String to;
    private String text;

    public Letter(String from, String to){
        this.from = from;
        this.to = to;
        this.text = "";
    }

    public void addLine(String line){
        text = text + String.format("%s\n", line);
    }

    public String getText() {
        return String.format("Dear %s\n\n%s\nSincerely,\n\n%s", this.from, this.text, this.to);
    }
}
