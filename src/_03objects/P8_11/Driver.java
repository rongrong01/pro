package _03objects.P8_11;

/**
 * Created by Haining on 10/16/16.
 */
public class Driver {
    public static void main(String[] args) {
        Letter letter = new Letter("John","Mary");
        letter.addLine("I am sorry we must part.");
        letter.addLine("I wish you all the best.");
        System.out.println(letter.getText());
    }
}
