package _03objects.P8_15;

/**
 * Created by Haining on 10/16/16.
 */
public class Cone {
    private double radius;
    private double height;

    public Cone(double radius, double height){
        this.radius = radius;
        this.height = height;
    }

    public double coneVolume() {
        return (1.0 / 3.0) * Math.PI * radius * radius * height;
    }

    public double coneSurface() {
        double l = Math.sqrt((radius * radius) + (height * height));
        return Math.PI * radius * (radius + l);
    }
}
