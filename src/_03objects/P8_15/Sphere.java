package _03objects.P8_15;

/**
 * Created by Haining on 10/16/16.
 */
public class Sphere {
    private double radius;

    public Sphere(double radius){
        this.radius = radius;
    }

    public double sphereVolume() {
        return (4.0 / 3.0) * Math.PI * this.radius * this.radius * this.radius;
    }

    public double sphereSurface() {
        return 4 * Math.PI * this.radius * this.radius;
    }
}
