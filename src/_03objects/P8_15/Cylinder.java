package _03objects.P8_15;

/**
 * Created by Haining on 10/16/16.
 */
public class Cylinder {
    private double radius;
    private double height;

    public Cylinder(double radius, double height){
        this.radius = radius;
        this.height = height;
    }

    public double cylinderVolume() {
        return Math.PI * this.radius * this.radius * this.height;
    }

    public double cylinderSurface() {
        return 2 * Math.PI * this.radius * (this.radius + this.height);
    }
}
