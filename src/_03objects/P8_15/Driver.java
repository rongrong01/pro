package _03objects.P8_15;

import java.util.Scanner;

/**
 * Created by Haining on 10/16/16.
 */
//This approach is more object-oriented than P8.14, as it treats Sphere, Cyliner, and Cone as individual object.
public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter radius: ");
        double radius = in.nextDouble();
        System.out.print("Please enter height: ");
        double height = in.nextDouble();
        in.close();

        Sphere sphere = new Sphere(radius);
        Cylinder cylinder = new Cylinder(radius,height);
        Cone cone = new Cone(radius,height);

        double sphereVolume = sphere.sphereVolume();
        double sphereSurface = sphere.sphereSurface();
        double cylinderVolume = cylinder.cylinderVolume();
        double cylinderSurface = cylinder.cylinderSurface();
        double coneVolume = cone.coneVolume();
        double coneSurface = cone.coneVolume();

        System.out.printf("Sphere Volume is: %.2f\n", sphereVolume);
        System.out.printf("Sphere Surface is: %.2f\n", sphereSurface);
        System.out.printf("Cylinder Volume is: %.2f\n", cylinderVolume);
        System.out.printf("Cylinder Surface is: %.2f\n", cylinderSurface);
        System.out.printf("Cone Volume is: %.2f\n", coneVolume);
        System.out.printf("Cone Surface is: %.2f\n", coneSurface);

    }

}
