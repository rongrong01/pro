package _07blackjack.blackjack;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by Haining on 11/13/16.
 */
public class TableLayout {
    private JPanel mPanel;
    private JButton $100BetButton;
    private JButton $50BetButton;
    private JButton $20BetButton;
    private JButton $10BetButton;
    private JButton hitButton;
    private JTextField playerBetField;
    private JTextField playerBankroll;
    private JTextArea textArea1;
    private JButton stayButton;
    private JButton startGameButton;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Blackjack Game");
        frame.setContentPane(new TableLayout().mPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(1200, 600);
        frame.setVisible(true);
    }

    public TableLayout(){
        Player player = new Player();
        CardShoe cardShoe = new CardShoe();
        Hand hand = new Hand();
        startUp(player);

        class BetListener implements ActionListener{
            int betSize;
            Player player = new Player();

            public BetListener(int betSize, Player player){
                this.betSize = betSize;
                this.player = player;
            }

            public void actionPerformed(ActionEvent e) {
                if (!player.isGameInProgress()){
                    player.addBet(betSize);
                    printMoney(player);
                }
            }
        }

        $10BetButton.addActionListener(new BetListener(10, player));
        $20BetButton.addActionListener(new BetListener(20, player));
        $50BetButton.addActionListener(new BetListener(50, player));
        $100BetButton.addActionListener(new BetListener(100, player));

        class StartGameListener implements ActionListener{
            Player player = new Player();
            Hand hand = new Hand();
            CardShoe cardShoe = new CardShoe();


            private StartGameListener(Player player, Hand hand, CardShoe cardShoe){
                this.player = player;
                this.hand = hand;
                this.cardShoe = cardShoe;
            }

            public void actionPerformed(ActionEvent e){
                    player.setGameInProgress(true);
                    if(cardShoe.getCardCount()>52){
                        textArea1.append("\n`-`-`-Shuffling deck`-`-`");
                        cardShoe.shuffle();
                    }
                    textArea1.append("\n************\nDealing cards.");
                    hand.resetHand();
                    hand.addPlayerCard(cardShoe.getCard());
                    hand.addPlayerCard(cardShoe.getCard());
                    hand.addCompCard(cardShoe.getCard());
                    printCards(hand);
                    if(hand.hardScorePlayer()==21){
                        textArea1.append("\nBlackjack!!! Pay " + (player.getBet() * 2) + ".");
                        player.playerBlackjack();
                        printMoney(player);
                    }

                    else{
                        textArea1.append("\nHit or stay?\n");
                    }

                }
            }

        startGameButton.addActionListener(new StartGameListener(player, hand, cardShoe));

        class HitListener implements ActionListener{
            Player player = new Player();
            Hand hand = new Hand();
            CardShoe cardShoe = new CardShoe();


            private HitListener(Player player, Hand hand, CardShoe cardShoe){
                this.player = player;
                this.hand = hand;
                this.cardShoe = cardShoe;
            }

            public void actionPerformed(ActionEvent e){
                if(player.isGameInProgress()){
                    hand.addPlayerCard(cardShoe.getCard());
                    printCards(hand);
                    if(hand.softScorePlayer()>21){
                        //Check if the player has lost after the card was dealt.
                        textArea1.append("\nOver 21, you lose.");
                        player.playerLoses();
                        printMoney(player);
                    }
                    else{
                        textArea1.append("\nHit or stay?");
                    }
                }
            }
        }

        hitButton.addActionListener(new HitListener(player, hand, cardShoe));

        class StayListener implements ActionListener{
            Player player = new Player();
            Hand hand = new Hand();
            CardShoe cardShoe = new CardShoe();


            private StayListener(Player player, Hand hand, CardShoe cardShoe){
                this.player = player;
                this.hand = hand;
                this.cardShoe = cardShoe;
            }

            public void actionPerformed(ActionEvent e){
                if(player.isGameInProgress()){

                    //Computer must hit on soft 17 per instruction.
                    while(hand.winner() == 0){
                        hand.addCompCard(cardShoe.getCard());
                        printCards(hand);
                        printScore(hand);
                    }

                    if(hand.winner() == 3){
                        printCards(hand);
                        textArea1.append("\nPush--no winner yet.");
                        player.push();
                    }
                    else if(hand.winner() == 1){
                        printCards(hand);
                        textArea1.append("\nComputer wins.");
                        player.playerLoses();
                    }
                    else {
                        printCards(hand);
                        textArea1.append("\nPlayer wins.");
                        player.playerWin();
                    }
                    printMoney(player);

                }
            }
        }

        stayButton.addActionListener(new StayListener(player, hand, cardShoe));
    }


    private void startUp(Player player){
        playerBankroll.setText(player.getPlayerTotalStr());
        textArea1.setText("Welcome to the table. \nBlackjack pays 2:1. No split, double down, or insurance.\n Good luck!");
    }

    public void printCards(Hand hand){
        textArea1.setText("\nPlayer's cards:\n");
        for (int i = 0; i < hand.getCardsPlayer().size(); i++) {
            textArea1.append(hand.getCardsPlayer().get(i).getStrCardNum().toUpperCase() + "of"  +
                    hand.getCardsPlayer().get(i).getCardSuit().substring(0,1).toUpperCase()+ " ");
        }

        textArea1.append("\nComputer's cards:\n");
        for (int i = 0; i < hand.getCardsComputer().size(); i++) {
            textArea1.append(hand.getCardsComputer().get(i).getStrCardNum().toUpperCase() + "of" +
                    hand.getCardsComputer().get(i).getCardSuit().substring(0,1).toUpperCase() + " ");
        }

        printScore(hand);

    }

    public void printScore(Hand hand){
        if ((hand.softScorePlayer() != hand.hardScorePlayer()) && hand.hardScorePlayer()<22){
            textArea1.append("\n***Player score: " + hand.hardScorePlayer() + " or " + hand.softScorePlayer() +
                    ".***");

        }
        else{
            textArea1.append("\n***Player score: " + hand.softScorePlayer() + ".***");
        }

        if (hand.hardScoreComp()>17 && hand.hardScoreComp()<22){
            textArea1.append("\n###Computer score: " + hand.hardScoreComp() + ".###");

        }
        else{
            textArea1.append("\n###Computer score: " + hand.softScoreComputer() + ".###");
        }
    }

    //Print the bankroll.
    public void printMoney(Player player){
        playerBetField.setText(player.getBetStr());
        playerBankroll.setText(player.getPlayerTotalStr());
    }
}
