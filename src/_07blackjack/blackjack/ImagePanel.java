package _07blackjack.blackjack;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by Haining on 11/13/16.
 */
//PNG-cards copied from labjava.package lec07.glab.jpanel_gui.imgs.
//Still cannot figure out how to upload .png to intelliJ rather than from local path.
public class ImagePanel {
    private BufferedImage image;
    private int upper;
    private int right;
    public ImagePanel(int upper, int right, Card card) {
        String strImage = "\\src\\_07blackjack\\blackjack\\PNG-cards\\";
        strImage +=card.getStrCardNum() + "_of_"+card.getCardSuit()+".png";
        System.out.println(strImage);
        //image = SoundImageUtils.genBuffImage("\\proBlackjack\\src\\blackjack\\PNG-cards-1.3\\king_of_clubs.png");
        image = SoundImage.genBuffImage(strImage);
        this.right = right;
        this.upper = upper;
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawImage(image, right, upper, 150,216, null); // see javadoc for more info on the parameters
    }
}
