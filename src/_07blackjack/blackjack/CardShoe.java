package _07blackjack.blackjack;

/**
 * Created by Haining on 11/13/16.
 */

/**This replicates a card Shoe that is used to deal the cards. There are 6 decks. We use a 0-1 flag to mark if the
 *card has been previously selected (0-not selected, 1-selected). If a card is not selected before, return card
 * and update flag.
 */
public class CardShoe {
    private int[] cardsDeck = new int[52*6];
    private int cardCount;

    //Shuffle card
    public void shuffle(){
        for (int i = 0; i <cardsDeck.length; i++) {
            cardsDeck[i]  = 0;
        }
        cardCount = 0;
    }

    //Select cards
    public Card getCard(){
        while(true){
            int drawCard = (int) (Math.random()*52*6);
            if (cardsDeck[drawCard] == 0){
                cardsDeck[drawCard] = 1;
                cardCount += 1;
                return new Card(drawCard%52);
            }
        }
    }

    //return how many cards have been selected
    public int getCardCount(){
        return cardCount;
    }
}
