package _07blackjack.blackjack;

import java.util.ArrayList;

/**
 * Created by Haining on 11/13/16.
 */
public class Player {
    private String player;//player's name
    private int playerTotal;//amount of money in the player's bankroll
    private int bet;
    private boolean gameInProgress;

    public Player(){
        player = "Player";
        playerTotal = 1000;
        bet = 0;
        gameInProgress = false;
    }

    public String getPlayer() {
        return player;
    }

    public double getPlayerTotal() {
        return playerTotal;
    }

    public boolean isGameInProgress() {
        return gameInProgress;
    }

    public void setGameInProgress(boolean gameInProgress) {
        this.gameInProgress = gameInProgress;
    }

    public String getPlayerTotalStr() {
        return "$" + (int) playerTotal;
    }

    //Player wins, the player total is incremented by the bet and original money is returned.
    public void playerWin(){
        playerTotal += bet*2;
        bet = 0;
        gameInProgress = false;
    }

    //Player gets blackjack, the player total is incremented by 2 * the bet and the original bet is returned.
    public void playerBlackjack(){
        playerTotal += bet*3;
        bet = 0;
        gameInProgress = false;
    }

    //Player loses. Reset bet variable to 0.
    public void playerLoses(){
        bet = 0;
        gameInProgress = false;
    }

    //A push.  No winner, player get the original bet, reset the bet.
    public void push(){
        playerTotal += bet;
        gameInProgress = false;
        bet = 0;
    }

    //Add bet after checking that the player has enough money.
    public void addBet(int bet){
        if (bet > playerTotal){
            return;
        }
        else {
            this.bet += bet;
            this.playerTotal -= bet;
        }

    }

    public int getBet(){
        return bet;
    }

    public String getBetStr(){
        return "$" + bet;
    }
}
