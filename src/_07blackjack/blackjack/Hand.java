package _07blackjack.blackjack;

import java.util.ArrayList;

/**
 * Created by Haining on 11/13/16.
 */
public class Hand {
    private ArrayList<Card> cardsPlayer = new ArrayList<>();
    private ArrayList<Card> cardsComputer = new ArrayList<>();
    private boolean hand;

    public Hand() {
        hand = true;
    }

    public void addPlayerCard(Card card){
        cardsPlayer.add(card);
    }

    public ArrayList<Card> getCardsPlayer() {
        return cardsPlayer;
    }

    public ArrayList<Card> getCardsComputer() {
        return cardsComputer;
    }

    public void addCompCard(Card card){
        cardsComputer.add(card);
    }

    //Calculate palyer hard score, which is soft score + 10.
    public int hardScorePlayer(){
        int score = 0;
        int aces = 0;
        for (int i = 0; i < cardsPlayer.size(); i++) {
            score += cardsPlayer.get(i).getCardVal();
            if (cardsPlayer.get(i).getCardVal() == 1){
                aces += 1;
            }
        }
        if (aces>0){
            return score + 10;
        }
        else{
            return score;
        }
    }

    //Calculate player soft score, which is the score with no aces as 11s.
    public int softScorePlayer(){
        int score = 0;
        int aces = 0;
        for (int i = 0; i < cardsPlayer.size(); i++) {
            score += cardsPlayer.get(i).getCardVal();
        }
        return score;
    }

    ////Calculate computer hard score, which is soft score + 10.
    public int hardScoreComp(){
        int score = 0;
        int aces = 0;
        for (int i = 0; i < cardsComputer.size(); i++) {
            score += cardsComputer.get(i).getCardVal();
            if (cardsComputer.get(i).getCardVal() == 1){
                aces += 1;
            }
        }
        if (aces>0){
            return score + 10;
        }
        else{
            return score;
        }
    }

    //Calculate computer soft score, which is the score with no aces as 11s.
    public int softScoreComputer(){
        int score = 0;
        int aces = 0;
        for (int i = 0; i < cardsComputer.size(); i++) {
            score += cardsComputer.get(i).getCardVal();
        }
        return score;
    }

    public void resetHand(){
        cardsPlayer = new ArrayList<>();
        cardsComputer = new ArrayList<>();
    }


    public int winner(){
        int compScore;
        int playerScore;
        //Check to see if there is a winner.
        // 0 - no winner yet; 1- computer wins; 2 - player wins; 3 - a push

        //If player soft score >21, player lost, computer win.
        if(softScorePlayer() > 21){
            return 1;
        }
        //If computer soft score >21, computer lost, player win.
        else if (softScoreComputer() > 21) {
            return 2;
        }
        //If computer score is less than 17, there is no winner yet, game continues.
        else if(softScoreComputer()<17 || hardScoreComp()<18){
            return 0;
        }
        //If the computer soft score is greater than 17, compare the scores.
        else{
            if(Math.max(softScoreComputer(),hardScoreComp()) < 22){
                compScore = Math.max(softScoreComputer(),hardScoreComp());
            }
            else{
                compScore = softScoreComputer();
            }

            if(Math.max(softScorePlayer(),hardScorePlayer()) < 22){
                playerScore = Math.max(softScorePlayer(),hardScorePlayer());
            }
            else{
                playerScore = softScorePlayer();
            }

            if(compScore == playerScore ){
                return 3;
            }
            else if(compScore < playerScore){
                return 2;
            }
            else if(compScore > playerScore){
                return 1;
            }

        }
        return 0;
    }
}

