package _07blackjack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Haining on 11/13/16.
 */
public class YodaSpeakRecursive {
    public static void main(String[] args) {
    String yodaSpeak;
    Scanner in = new Scanner(System.in);
    System.out.println("Please enter a sentence to reformat as yoda speak:");
    yodaSpeak = in.nextLine();
    ArrayList<String> yodaSplit = new ArrayList<>(Arrays.asList(yodaSpeak.split(" ")));

    ArrayList<String> yodaRecursed = recurseYoda(yodaSplit);
        for(int i = 0; i < yodaRecursed.size(); i++) {
            System.out.print(yodaRecursed.get(i) + " ");
    }
}

    public static ArrayList<String> recurseYoda(List<String> aList){
        //base case
        if ((aList == null) || (aList.size() <= 1)) {
            return (ArrayList<String>) aList;
        }
        //recursive call
        String first = aList.get(0);
        aList.remove(0);
        ArrayList<String> newList = recurseYoda(aList);
        newList.add(first);
        return newList;
    }
}
