package _07blackjack;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Haining on 11/13/16.
 */
public class YodaSpeak {
    public static void main(String[] args) {
        String yodaSpeak;
        String[] yodaSplit;
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter a sentence to reformat as yoda speak:");
        yodaSpeak = in.nextLine();
        yodaSplit = yodaSpeak.split(" ");

        for (int i = yodaSplit.length-1; i >= 0 ; i--) {
            System.out.print(yodaSplit[i]);
            System.out.print(" ");
        }
    }
}
