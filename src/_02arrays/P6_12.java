package _02arrays;

/**
 * Created by Haining on 10/9/16.
 */
public class P6_12 {
    public static void main(String[] args) {
        final int size = 20;
        int[] values = generateDieTosses(size);
        run(values);
    }

    //print the die values, marking the runs by including them in parentheses
    public static void run(int[] values){
        boolean inRun = false;
        int previousValue = values[0];
        for (int i = 0; i < values.length - 1; i++){
            if (inRun) {
                if (values[i] != previousValue){
                    System.out.print(") ");
                    inRun = false;
                }
            } else{
                if (values[i] == values[i+1]){
                    System.out.print(" (");
                    inRun = true;
                } else{
                    System.out.print(" ");
                }
            }
            previousValue = values[i];
            System.out.print(values[i]);
        }
        //boundary coondition: now consider the last element in values
        if (inRun && values[values.length - 1] == previousValue) {
            System.out.print(" " + values[values.length - 1] + ")");
        } else if (inRun && values[values.length - 1] != previousValue) {
            System.out.print(") " + values[values.length - 1]);
        } else {
            System.out.print(" " + values[values.length - 1]);
        }
    }

    //generates a sequence of 20 random die tosses in an array
    public static int[] generateDieTosses(int size) {
        int[] tosses = new int[size];
        for (int i = 0; i < size; i++) {
            tosses[i] = (int) (Math.random() * 6 + 1);
        }
        return tosses;
    }
}
