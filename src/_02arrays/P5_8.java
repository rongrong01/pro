package _02arrays;

import java.util.Scanner;
import java.util.Random;

/**
 * Created by Haining on 10/9/16.
 */
public class P5_8 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter a word: ");

        while (!in.hasNext("exit!")) {
            String word = in.next();
            System.out.println("scrambled version: " + scramble(word));
        }

        in.close();
    }

    public static String scramble(String word){
        if(word.length() > 3) {
            //randomly generate two indices that are to be flipped in the word
            Random generator = new Random();
            int index1 = generator.nextInt(word.length() - 1) + 1;
            int index2 = generator.nextInt(word.length() - 1) + 1;

            //if they happen to be the same, we need to re-generate
            if (index1 == index2) {
                while (index1 == index2) {
                    index2 = generator.nextInt(word.length() - 1) + 1;
                }
            }

            if(index1 > index2) {
                return word.substring(0, index2) +
                        word.charAt(index1) +
                        word.substring(index2 + 1, index1) +
                        word.charAt(index2) +
                        word.substring(index1 + 1);
            }else{
                return word.substring(0, index1) +
                        word.charAt(index2) +
                        word.substring(index1 + 1, index2) +
                        word.charAt(index1) +
                        word.substring(index2 + 1);
            }
        }else{
            return word;
        }

    }
}

