package _02arrays;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Haining on 10/9/16.
 */
public class P6_28 {
    public static void main(String[] args) {
        //read input
        Scanner in = new Scanner(System.in);

        ArrayList<Integer> arrayList1 = new ArrayList<>();
        System.out.print("Please enter length of array list 1: ");
        int lenthOfArrayList1 = in.nextInt();
        for (int i = 0; i < lenthOfArrayList1; i++) {
            System.out.print("Enter element of array list 1: ");
            arrayList1.add(in.nextInt());
        }

        ArrayList<Integer> arrayList2 = new ArrayList<>();
        System.out.print("Please enter length of array list 2: ");
        int lenthOfArrayList2 = in.nextInt();
        for (int i = 0; i < lenthOfArrayList2; i++) {
            System.out.print("Enter element of array list 2: ");
            arrayList2.add(in.nextInt());
        }

        in.close();

        ArrayList<Integer> newArrayList = mergeSorted(arrayList1, arrayList2);
        for (int i = 0; i < newArrayList.size(); i++) {
            System.out.print(newArrayList.get(i) + " ");
        }
    }

    //merge 2 sorted array lists
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> newArrayList = new ArrayList<Integer>();
        while (a.size() > 0 || b.size() > 0) {
            if (a.get(0) < b.get(0)) {
                newArrayList.add(a.get(0));
                a.remove(a.get(0));
            } else if (b.get(0) < a.get(0)) {
                newArrayList.add(b.get(0));
                b.remove(b.get(0));
            } else {  //if a.get(0) == b. get(0), add both to newArrayList, and then remove them from original arrays
                newArrayList.add(a.get(0));
                a.remove(a.get(0));
                newArrayList.add(b.get(0));
                b.remove(b.get(0));
            }
        }

        return newArrayList;
    }
}
