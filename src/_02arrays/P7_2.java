package _02arrays;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
/**
 * Created by Haining on 10/9/16.
 */
public class P7_2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter name of file to read: ");
        String fileNameRead = in.next();
        System.out.print("Enter name of file to write to: ");
        String fileNameWrite = in.next();
        in.close();

        File fileRead = new File(fileNameRead);
        Scanner filein = null;
        File fileWrite = new File(fileNameWrite);
        PrintWriter fileout = null;

        try{
            filein = new Scanner(fileRead);
            fileout = new PrintWriter(fileWrite);
        } catch (FileNotFoundException e){
            System.out.println("File not found.");
        }

        int lineNumber = 1;
        while (filein.hasNextLine()) {
            String line = in.nextLine();
            fileout.write(String.format("/* %d */ %s\n", lineNumber, line));
            lineNumber++;
        }

        fileout.close();
        filein.close();

        System.out.println("Done.");
    }
}
