package _02arrays;

import java.util.Scanner;

/**
 * Created by Haining on 10/9/16.
 */
public class P6_23 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter length of sequence: ");
        int lengthOfSequence = in.nextInt();
        int[] sequence = new int[lengthOfSequence];
        String[ ] captions = new String[lengthOfSequence];
        for (int i = 0; i < lengthOfSequence; i++) {
            System.out.print("Please enter canption: ");
            captions[i] = in.next();
            System.out.print("Please enter value: ");
            sequence[i] = in.nextInt();
        }
        printChart(sequence, captions);
        in.close();
    }

    public static void printChart(int[] sequences, String[] captions){
        int maxValue = findMax(sequences);
        for (int i = 0; i <sequences.length; i++){
            System.out.printf("%10s ", captions[i]);
            int proportion = (sequences[i]/maxValue) * 40;
            //I do not know why the proporation does not work
            for (int j = 0; j < proportion; j++){
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static int findMax(int[] sequences){
        int maxValue = sequences[0];
        for (int i = 1; i < sequences.length; i++){
            if (sequences[i] > maxValue){
                maxValue = sequences[i];
            }
        }
        return maxValue;
    }
}
