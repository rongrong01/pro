package _02arrays;

import java.util.Scanner;

/**
 * Created by Haining on 10/9/16.
 */
public class P5_20 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter a year: ");
        int year = in.nextInt();
        System.out.println(isLeapYear(year));
        in.close();
    }

    public static boolean isLeapYear(int year) {
        if (year % 400 == 0) {
            return true;
        }
        else if (year % 4 == 0 && year % 100 != 0) {
            return true;
        }
        return false;
    }
}
