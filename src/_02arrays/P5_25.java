package _02arrays;

import java.util.Scanner;

/**
 * Created by Haining on 10/9/16.
 */
public class P5_25 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter a zip code: ");
        int zipCode = in.nextInt();
        in.close();
        int checkDigit = checkDigit(zipCode);
        printBarCode(zipCode);

    }
    public static int checkDigit(int zipCode) {
        int sum = 0;
        while (zipCode > 0) {
            int remaining = zipCode % 10;
            zipCode = zipCode / 10;
            sum += remaining;
        }
        return 10 - (sum % 10);
    }

    public static void printDigit(int digit) {
        if (digit == 0) {
            System.out.print("||:::");
        }
        if (digit == 1) {
            System.out.print(":::||");
        }
        if (digit == 2) {
            System.out.print("::|:|");
        }
        if (digit == 3) {
            System.out.print("::|:|");
        }
        if (digit == 4) {
            System.out.print(":|::|");
        }
        if (digit == 5) {
            System.out.print(":|:|:");
        }
        if (digit == 6) {
            System.out.print(":||::");
        }
        if (digit == 7) {
            System.out.print("|:::|");
        }
        if (digit == 8) {
            System.out.print("|::|:");
        }
        if (digit == 9)
            System.out.print("|:|::");
    }

    public static void printBarCode(int zipCode) {
        int zip = zipCode;
        System.out.print("|");
        int divisor = 10000;

        for (int i = 0; i < 5; i++) {
            int currentDigit = zip / divisor;
            printDigit(currentDigit);
            zip =zip % divisor;
            divisor = divisor / 10;
        }

        int checkDigit = checkDigit(zipCode);
        printDigit(checkDigit);
        System.out.print("|");
    }

}
