package _06design.P12_6;

import java.util.Scanner;

/**
 * Created by Haining on 11/6/16.
 */
public class Tester {
    private Student student;
    private int level;
    private int tries = 0; //only 2 tries are allowed
    private Scanner in = new Scanner(System.in);
    private Question question;

    public Tester(Student student){
        this.student = student;
        level = student.getStudentLevel();

    }

    public void runTester(){
        level = student.getStudentLevel();
        Question question = new Question(level);
        while (tries < 2){
            System.out.printf(question.getQuestion());
            if (question.checkAnswer(getAnswerText())){
                student.incrementStudentLevel();
                tries = 0;
                return;
            }
            else{
                System.out.println("Answer is not correct. Please try again.");
                tries += 1;
            }
        }
        tries = 0;
        System.out.println("Second try is not correct. Next question.");
    }

    public int getAnswerText(){
        int guess;
        try{
            guess = Integer.parseInt(in.next());
            return guess;
        }catch(NumberFormatException e){
            System.out.println("Not a valid guess.");
            return -1;
        }
    }
}
