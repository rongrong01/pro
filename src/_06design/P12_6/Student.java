package _06design.P12_6;

/**
 * Created by Haining on 11/6/16.
 */
public class Student {

    private String studentName;
    private int studentLevel;


    public int getStudentLevel() {
        return studentLevel;
    }

    public void incrementStudentLevel() {
        this.studentLevel = studentLevel + 1;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
}
