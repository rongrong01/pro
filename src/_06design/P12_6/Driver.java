package _06design.P12_6;

import java.util.Scanner;

/**
 * Created by Haining on 11/6/16.
 */
public class Driver {
    public static void main(String[] args) {
        Student student = new Student();
        Scanner in = new Scanner(System.in);
        Tester tester = new Tester(student);
        System.out.printf("Please enter your name:");
        student.setStudentName(in.nextLine());

        while(student.getStudentLevel() < 3){
            System.out.println("Level: " + (student.getStudentLevel()+1));
            tester.runTester();
        }

        System.out.println("Congratulations! You passed all the levels!");

    }
}
