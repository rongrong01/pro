package _06design.P12_6;

/**
 * Created by Haining on 11/6/16.
 */
public class Question {
    private int level; //3 levels in total
    private int number1; //airthmetic input number 1
    private int number2; //airthmetic input number 2
    private int answer;
    private String question;

    public Question(int level){

        this.level = level;
        if (level == 0){
            //Choose a random number between 0 and 9.  Then choose a second one such that the sum is less than 10.
            number1 = (int) (Math.random()*10);
            number2 = (int) (Math.random()*(10-number1));
            answer = number1 + number2;
        }

        if (level == 1){
            //Choose two arbitrary one digit numbers.
            number1 = (int) (Math.random()*10);
            number2 = (int) (Math.random()*10);
            answer = number1 + number2;
        }

        if (level == 2){
            //Choose two random numbers.
            number1 = (int) (Math.random()*10);
            number2 = (int) (Math.random()*10);
            answer = Math.abs(number1-number2);
        }
    }

    public String getQuestion(){
        if (level == 0 || level == 1){
            question = "What is the answer to " + number1 + " + " + number2 + ": ";
        }
        if (level == 2){
            question = "What is the answer to " + Math.max(number1,number2) + " - " + Math.min(number1,number2) + ": ";

        }

        return question;
    }

    public boolean checkAnswer(int testAnswer){
        if (answer == testAnswer){
            return true;
        }
        return false;
    }

    public int getAnswer(){
        return answer;
    }
}
