package _06design.P12_8;

/**
 * Created by Haining on 11/6/16.
 */
public class Product {
    private String productName;
    private int count;
    private double price;


    public Product(String name, int count, double price){
        this.productName = name;
        this.count = count;
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void addCount(int count) {
        this.count += count;
        this.count = Math.max(0, this.count);
    }

    public void buyItem(){
        this.count -= 1;
    }

    public double getPrice() {
        return price;
    }

    public String getProductName() {
        return productName;
    }
}
