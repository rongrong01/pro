package _06design.P12_8;

import java.util.ArrayList;

/**
 * Created by Haining on 11/6/16.
 */
public class VendingMachine {
    private ArrayList<Product> products = new ArrayList<Product>();
    private Coin coinCounter = new Coin();

    //Add a product to the machine.
    public void addProduct(Product product){
        products.add(product);
    }

    //Add coins to the machine.
    public void addCoins(Coin c){
        coinCounter.addCoins(c);
    }

    //Check how much money is in the machine.
    public double checkMoney(){
        return coinCounter.getTotal();
    }

    //Return the array of products.
    public ArrayList<Product> getProducts(){
        return products;
    }

    //Get the inventory.
    public int getProductInventory(int i){
        return products.get(i).getCount();
    }

    //Reset the coin counter.
    public void collectMoney(){
        coinCounter.resetCoins();
    }

    //Add inventory
    public void addInventory(int choice, int addNumber){
        products.get(choice).addCount(addNumber);
    }

    //Return the price of an item
    public double getCost(int item){
        return products.get(item).getPrice();
    }

    //Get the name of a product
    public String getName(int item){
        return products.get(item).getProductName();
    }

    //Buy an item from the product list.
    public void buyItem(int item){
        products.get(item).buyItem();
    }
}
