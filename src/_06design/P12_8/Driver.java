package _06design.P12_8;

/**
 * Created by Haining on 11/6/16.
 */
public class Driver {
    public static void main(String[] args) {
        VendingMachine vendingMachine = new VendingMachine();

        vendingMachine.addProduct(new Product("Coke", 12, 1.25));
        vendingMachine.addProduct(new Product("Sprite", 10, 1.25));
        vendingMachine.addProduct(new Product("Chips", 14, 0.75));

        Coin startMoney = new Coin(3,5,4,10);
        vendingMachine.addCoins(startMoney);

    }
}
