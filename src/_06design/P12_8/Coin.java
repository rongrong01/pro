package _06design.P12_8;

/**
 * Created by Haining on 11/6/16.
 */
public class Coin {
    public static double PENNY = .01;
    public static double NICKEL = .05;
    public static double DIME = .1;
    public static double QUARTER = .25;
    private int[] coins = new int[4];
    private double total = 0;

    public Coin(){}

    public Coin(int penny, int nickel, int dime, int quarter) {
        coins[0] = penny;
        coins[1] = nickel;
        coins[2] = dime;
        coins[3] = quarter;
    }

    //Methods to add the different coin values to the array of count of each coin type.
    public void addPenny(){
        coins[0] += 1;
    }

    public void addNickel(){
        coins[1] += 1;
    }

    public void addDime(){
        coins[2] += 1;
    }

    public void addQuarter(){
        coins[3] += 1;
    }

    //Calculate the total value in the coin holder.
    public double getTotal(){
        total = coins[0]*.01 + coins[1]*.05 + coins[2]*.1 + coins[3]*.25;
        return total;
    }

    public int[] getCoins() {
        return coins;
    }

    //Zero out the array; this method is what happens when the money is collected out of the coin holder.
    public void resetCoins(){
        for (int i = 0; i < coins.length; i++) {
            coins[i] = 0;
        }
    }

    //This adds an entire coin array to the existing coin.
    public void addCoins(Coin coin){
        int[] coinArray = coin.getCoins();
        for (int i = 0; i <coinArray.length ; i++) {
            coins[i] += coinArray[i];
        }
    }

}
