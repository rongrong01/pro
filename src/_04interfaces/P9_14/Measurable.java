package _04interfaces.P9_14;

/**
 * Created by Haining on 10/23/16.
 */
public interface Measurable {
    double getMeasure();
}
