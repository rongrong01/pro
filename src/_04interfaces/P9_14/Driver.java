package _04interfaces.P9_14;

/**
 * Created by Haining on 10/23/16.
 */
public class Driver {
    public static void main(String[] args) {
        Measurable[] sodaCans = {
                new SodaCan(1, 2),
                new SodaCan(3, 4),
                new SodaCan(1, 6),
                new SodaCan(2, 1),
        };


        System.out.printf("Average Surface Area of SodaCans: %.2f\n", averageSurfaceAreaOfSodaCans(sodaCans));
    }

    public static double averageSurfaceAreaOfSodaCans(Measurable[] sodaCans) {
        double average = 0;

        for (int i = 0; i < sodaCans.length; i++) {
            average += sodaCans[i].getMeasure();
        }

        return average / sodaCans.length;
    }

}
