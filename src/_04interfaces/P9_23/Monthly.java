package _04interfaces.P9_23;

/**
 * Created by Haining on 10/23/16.
 */
public class Monthly extends Appointment{

    public Monthly(String description, int year, int month, int day){
        super(description, year, month, day);
    }

    @Override
    // check if the day of month matches the one provided, as it is monthly appointment.
    public boolean occursOn(int year, int month, int day) {
        return day == super.getAppointmentday();
    }
}
