package _04interfaces.P9_23;

import java.util.*;

/**
 * Created by Haining on 10/23/16.
 */
public class Appointment {
    private String description;
    private int appointmentyear;
    private int appointmentmonth;
    private int appointmentday;

    public Appointment(String description, int year, int month, int day){
        this.description = description;
        this.appointmentyear = year;
        this.appointmentmonth = month;
        this.appointmentday = day;
    }

    public boolean occursOn(int year, int month, int day){
        return ((appointmentyear == year) && (appointmentmonth == month) && (appointmentday == day));
    }

    public String getDescription(){
        return description;
    }

    public int getAppointmentyear(){
        return appointmentyear;
    }

    public int getAppointmentmonth() {
        return appointmentmonth;
    }

    public int getAppointmentday() {
        return appointmentday;
    }
}
