package _04interfaces.P9_23;

/**
 * Created by Haining on 10/23/16.
 */
public class Daily extends Appointment {

    public Daily(String description, int year, int month, int day){
        super(description, year, month, day);
    }


    @Override
    // Return true if a valid date is passed through the method, as it is daily appointment, and it will happen every day.
    public boolean occursOn(int year, int month, int day) {
        return true;
    }
}
