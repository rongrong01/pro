package _04interfaces.P9_23;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Haining on 10/23/16.
 */
public class Driver {
    public static void main(String[] args) throws IOException{

        ArrayList<Appointment> myAppointments = new ArrayList<>();
        Monthly mortgage = new Monthly("Pay mortgage", 2016, 11, 10);
        Daily alarm = new Daily("Wake up alarm", 2016, 10, 23);
        Onetime condoClosing = new Onetime("Close condo", 2016, 10,5);
        myAppointments.add(mortgage);
        myAppointments.add(alarm);
        myAppointments.add(condoClosing);

        //menu method to go to different choice based on user input
        int choice = menu();
        while (choice != 0){
            if (choice == 1){
                printAppointment(myAppointments);
                choice = menu();
            } else if (choice == 2){
                addAppointment(myAppointments);
                choice = menu();
            } else if (choice ==3){
                save(myAppointments);
                choice = menu();
            } else if (choice == 4){
                load(myAppointments);
                choice = menu();
            }
        }
    }

    public static int menu(){
        String myChoice;
        System.out.println("\nChoose one of the following.");
        System.out.println("1. Print list of appointments.");
        System.out.println("2. Add an appointment.");
        System.out.println("3. Save an appointment calendar to file.");
        System.out.println("4. Load an appointment from a file.");
        System.out.println("Any other key to exit.");
        System.out.println("Choice: ");
        Scanner in = new Scanner(System.in);
        myChoice = in.next();
        if(myChoice.equals("1") || myChoice.equals("2") || myChoice.equals("4") || myChoice.equals("5")){
            return Integer.parseInt(myChoice);
        }
        else return 0;
    }

    //Print a list of the appointments.
    public static void printAppointment(ArrayList<Appointment> appointmentList){
        for (int i = 0; i < appointmentList.size(); i++){
            System.out.println((i+1) + ":" + appointmentList.get(i).getDescription() + " " + appointmentList.get(i).getAppointmentyear() +
            "-" + appointmentList.get(i).getAppointmentmonth() + "-" + appointmentList.get(i).getAppointmentday());
        }
    }

    //Add an appointment to the list.
    public static void addAppointment(ArrayList<Appointment> appointmentList){
        String type;
        String description;
        String sYear;
        String sMonth;
        String sDay;
        int year;
        int month;
        int day;

        Scanner in = new Scanner(System.in);
        System.out.println("Enter appt type (daily, monthly, onetime):");
        type = in.next().toLowerCase();
        System.out.println("Enter a string description:");
        description = in.next();
        System.out.println("Enter a year:");
        sYear = in.next();
        System.out.println("Enter a month:");
        sMonth = in.next();
        System.out.println("Enter a date:");
        sDay = in.next();

        //Check if inputs are ints.
        if(isInt(sDay) && isInt(sMonth) && isInt(sYear)){
            year = Integer.parseInt(sYear);
            month = Integer.parseInt(sMonth);
            day = Integer.parseInt(sDay);
        }

        else{
            System.out.println("Not valid inputs.  Please try again.");
            return;
        }

        //Check if the dates are valid.
        if(!validate(year, month, day)){
            System.out.println("Not valid inputs.  Please try again.");
            return;
        }

        //Create new object.
        //System.out.println(type.equals("daily"));
        if(type.equals("daily")){
            Daily d = new Daily(description, year, month, day);
            appointmentList.add(d);
            System.out.println("Daily appointment added.");
        }

        else if(type.equals("monthly")){
            Monthly m = new Monthly(description, year, month, day);
            appointmentList.add(m);
            System.out.println("Monthly appointment added.");

        }
        else if(type.equals("onetime")){
            Onetime o = new Onetime(description, year, month, day);
            appointmentList.add(o);
            System.out.println("Onetime appointment added.");

        } else{
            System.out.println("Not a valid type.");
            return;
        }
    }

    public static boolean validate(int year, int month, int day){
        if(year < 0){
            return false;
        }
        else if(month < 1 || month > 12){
            return false;
        }
        else if(day < 1 || day > 31){
            return false;
        }
        else if((month == 4 || month == 6 || month == 9 ||month == 11) && day > 30){
            return false;
        }
        else if((month == 2 && day > 29)){
            return false;
        } else{
            return true;
        }
    }

    public static boolean isInt( String input ){
        try {
            Integer.parseInt( input );
            return true;
        }
        catch( NumberFormatException e ) {
            return false;
        }
    }

    public static void save(ArrayList<Appointment> appointmentList){
        try {
            //Write the appointment calendar to file using PrintWriter.
            PrintWriter out = new PrintWriter("appointments.txt");

            //Use a for loop to loop through and print to the file.
            for (int i = 0; i < appointmentList.size() ; i++) {
                out.println(appointmentList.get(i).getDescription() + "," + appointmentList.get(i).getAppointmentyear() + "," +
                        appointmentList.get(i).getAppointmentmonth() + "," + appointmentList.get(i).getAppointmentday() + "\n");
            }

            out.close();

            System.out.println("Write to file.");

        } catch (IOException e) {
            System.out.println("Not successfully written.");
            e.printStackTrace();
        }
    }

    public static void load(ArrayList<Appointment> appointmentList) throws IOException{
        try{
            //Check what type of appointment to create.
            String apptType;
            String strInputFile;
            String apptInfo;
            System.out.print("Enter type of appointment to create Daily (d), Monthly (m), One-time (o):");
            Scanner in = new Scanner(System.in);
            apptType = in.nextLine().toLowerCase();
            //Get file name, open file.
            System.out.println("Enter a file from which to input data:");
            strInputFile = in.nextLine();
            File inputFile = new File(strInputFile);
            Scanner scFile = new Scanner(inputFile);
            //Read in first line of file and split it into pieces based on commas.
            apptInfo = scFile.nextLine();
            String[] splitStr = apptInfo.split(",");
            //Assign the appointment to the file based on type specified.
            System.out.println(apptType);
            if (apptType.equals("d")){
                Daily newD = new Daily(splitStr[0], Integer.parseInt(splitStr[1]), Integer.parseInt(splitStr[2]),
                        Integer.parseInt(splitStr[3]));
                appointmentList.add(newD);
            }
            else if (apptType.equals("m")){
                Monthly newD = new Monthly(splitStr[0], Integer.parseInt(splitStr[1]), Integer.parseInt(splitStr[2]),
                        Integer.parseInt(splitStr[3]));
                appointmentList.add(newD);
            }
            else if (apptType.equals("o")){
                Onetime newD = new Onetime(splitStr[0], Integer.parseInt(splitStr[1]), Integer.parseInt(splitStr[2]),
                        Integer.parseInt(splitStr[3]));
                appointmentList.add(newD);
            }
            else{
                System.out.println("Appointment type is not valid.");
            }
            scFile.close();
        }

        catch (IOException e){
            System.out.println("Couldn't read from file.");
        }
    }


}
