package _04interfaces.P9_16;

import _04interfaces.P9_14.Measurable;

/**
 * Created by Haining on 10/23/16.
 */
public class Driver{
    public static void main(String[] args) {
        Country[] countries = {
                new Country("USA",9826630),
                new Country("Canada",9984670),
                new Country("China",9596960),
                new Country("Russia",17075200),
        };

        Country bigCountry = (Country) maximum(countries);

        for (int i = 0; i < countries.length; i++){
            System.out.println(countries[i].getNameOfCountry() + ": " + countries[i].getMeasure());
        }

        System.out.println("Country with the largest area: " + bigCountry.getNameOfCountry());
        System.out.println("Its area is: " + bigCountry.getMeasure());
    }

    public static Measurable1 maximum(Measurable1[] objects){
        int maxIndex = -1;
        double maxArea= 0;

        for (int i = 0; i < objects.length ; i++) {
            if (maxArea < objects[i].getMeasure()){
                maxIndex = i;
                maxArea = objects[i].getMeasure();
            }
        }

        return objects[maxIndex];
    }

}
