package _04interfaces.P9_16;

/**
 * Created by Haining on 10/23/16.
 */
public class Country implements Measurable1{
    String nameOfCountry;
    double areaOfCountry;

    public Country(String nameOfCountry, double areaOfCountry){
        this.nameOfCountry = nameOfCountry;
        this.areaOfCountry = areaOfCountry;
    }

    public String getNameOfCountry(){
        return this.nameOfCountry;
    }

    public double getMeasure(){
        return this.areaOfCountry;
    }
}
