package _04interfaces.P9_13;

/**
 * Created by Haining on 10/23/16.
 */
public class Driver {
    public static void main(String[] args) {
        LabeledPoint labeledPoint = new LabeledPoint(1,2, "point1");

        System.out.println(labeledPoint.toString());
    }
}
