package _04interfaces.P9_13;

import java.awt.*;

/**
 * Created by Haining on 10/23/16.
 */
public class LabeledPoint extends Point {
    private String label;

    public LabeledPoint(int x, int y, String lable){
        super(x,y);
        this.label = lable;
    }

    public String getLabel(){
        return this.label;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("[label=\"%s\"]", this.getLabel());
    }
}
