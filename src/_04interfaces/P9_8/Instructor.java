package _04interfaces.P9_8;

/**
 * Created by Haining on 10/23/16.
 */
public class Instructor extends Person{
    private double salary;

    public Instructor(String name, int yearOfBirth, double salary){
        super(name,yearOfBirth);
        this.salary = salary;
    }

    public double getSalary(){
        return this.salary;
    }

    @Override
    public String toString() {
        return super.toString() + " salary: " + this.getSalary();
    }

}
