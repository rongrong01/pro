package _04interfaces.P9_8;

/**
 * Created by Haining on 10/23/16.
 */
public class Student extends Person {
    private String major;

    public Student(String name, int yearOfBirth, String major){
        super(name,yearOfBirth);
        this.major = major;
    }

    public String getMajor(){
        return this.major;
    }

    @Override
    public String toString() {
        return super.toString() + " major: " + this.getMajor();
    }
}
