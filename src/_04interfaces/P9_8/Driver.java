package _04interfaces.P9_8;

/**
 * Created by Haining on 10/23/16.
 */
public class Driver {
    public static void main(String[] args) {
        Person myPerson = new Person("Alice", 1970);
        Student myStudent = new Student("Bob", 1990, "Math");
        Instructor myInstructor = new Instructor("John", 1980, 75000);

        System.out.println(myPerson.toString());
        System.out.println(myStudent.toString());
        System.out.println(myInstructor.toString());

    }
}
