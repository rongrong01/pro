package _04interfaces.P9_8;

/**
 * Created by Haining on 10/23/16.
 */
public class Person {
    private String name;
    private int yearOfBirth;

    public Person(String name, int yearOfBirth){
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    public String getName(){
        return this.name;
    }

    public int getYearOfBirth(){
        return this.yearOfBirth;
    }

    @Override
    public String toString(){
        return getClass().getName() +  " name: " + this.getName() + " year of birth: " + this.getYearOfBirth();
    }
}
