package _04interfaces.P9_10;

/**
 * Created by Haining on 10/23/16.
 */
public class Driver {
    public static void main(String[] args) {
        BetterRectangle betterRectangle = new BetterRectangle(0,0,2,1);

        System.out.println("Perimeter of Rectangle: " + betterRectangle.getPerimeter());

        System.out.println("Area of Rectangle: " + betterRectangle.getArea());
    }
}
