package _04interfaces.P9_10;

import java.awt.*;

/**
 * Created by Haining on 10/23/16.
 */
public class BetterRectangle extends Rectangle {

    public BetterRectangle(int x, int y, int width, int height){
        this.setLocation(x,y);
        this.setSize(width, height);
    }

    public double getPerimeter(){
        return 2 * super.getHeight() + 2 * super.getWidth();
    }

    public double getArea(){
        return super.getHeight() * super.getWidth();
    }



}
