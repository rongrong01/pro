package _01control;

import java.util.Scanner;

/**
 * Created by Haining on 10/2/16.
 */
public class P3_26 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a number up to 3999: ");
        int number = input.nextInt();
        input.close();
        String roman = "";

        int digitinthousand = number/1000;

        if (digitinthousand == 3)
            roman = "MMM";
        else if (digitinthousand == 2)
            roman = "MM";
        else if (digitinthousand == 1)
            roman = "M";

        int digitinhundred = (number % 1000)/100;

        if (digitinhundred == 9) {
            roman += "CM";
        } else if (digitinhundred == 4) {
            roman += "CD";
        }else {
            if (digitinhundred >= 5) {
                roman += "D";
                digitinhundred -= 5;
            } if (digitinhundred == 3) {
                roman += "CCC";
            } else if (digitinhundred == 2) {
                roman += "CC";
            } else if (digitinhundred == 1) {
                roman += "C";
            }
        }

        int digitinten = (number % 100)/10;
        if (digitinten == 9) {
            roman += "XC";
        }else if (digitinten == 4) {
            roman += "XL";
        }else {
            if (digitinten >= 5) {
                roman += "L";
                digitinten -= 5;
            }  if (digitinten == 3) {
                roman += "XXX";
            } else if (digitinten == 2) {
                roman += "XX";
            } else if (digitinten == 1) {
                roman += "X";
            }
        }

        int digit = number % 10;
        if (digit == 9) {
            roman += "IX";
        }else if (digit == 4) {
            roman += "IV";
        }else {
            if (digit >= 5) {
                roman += "V";
                digit -= 5;
            } if (digit == 3) {
                roman += "III";
            } else if (digit == 2) {
                roman += "II";
            } else if (digit == 1) {
                roman += "I";
            }
        }
        System.out.println("Roman Numeral: " + roman);

    }

}
