package _01control;

import java.util.Scanner;

/**
 * Created by Haining on 10/2/16.
 */
public class P2_17 {
    public static void main(String[] args) {
        final int MINUTES_PER_HOUR = 60;
        final int MINUTES_PER_DAY = 24 * 60;
        Scanner time = new Scanner(System.in);
        System.out.print("Please enter the first time: ");
        int firstTime = time.nextInt();
        int firstTimeinMinutes = firstTime / 100 * MINUTES_PER_HOUR + firstTime % 100;

        System.out.print("Please enter the second time: ");
        int secondTime = time.nextInt();
        time.close();
        int secondTimeinMinutes = secondTime / 100 * MINUTES_PER_HOUR + secondTime % 100;

        int minutes = secondTimeinMinutes - firstTimeinMinutes;
        minutes = (minutes + MINUTES_PER_DAY) % (MINUTES_PER_DAY);
        int hours = minutes / 60;
        minutes = minutes % 60;
        System.out.println(hours + " hours " + minutes + " minutes");

    }

}
