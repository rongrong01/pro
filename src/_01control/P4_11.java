package _01control;

import java.util.Scanner;

/**
 * Created by Haining on 10/2/16.
 */
public class P4_11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Word: ");
        String word = input.next();
        input.close();
        int num_syllable = 0;
        boolean syllable = false;

        for (int i = 0; i < word.length();i++) {
            char cur_letter = Character.toLowerCase(word.charAt(i));
            if (cur_letter == 'a' || (cur_letter == 'e' && i != word.length() - 1) ||
                    cur_letter == 'i' || cur_letter == 'o' || cur_letter == 'u' || cur_letter == 'y') {
                if (!syllable) {
                    num_syllable += 1;
                    syllable = true;
                }
            } else {
                syllable = false;
            }
        }

            num_syllable = (num_syllable > 0)? num_syllable:1;
            System.out.println("Number of syllables: " + num_syllable);

    }
}
