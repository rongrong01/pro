package _01control;

import java.util.Scanner;

/**
 * Created by Haining on 10/2/16.
 */
public class P3_14 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the card notation: ");
        String card = input.next();
        input.close();
        String rank = card.substring(0, card.length() - 1);
        String suit = card.substring(card.length() - 1);

        if (rank.equals("A")) {
            rank = "Ace";
        } else if (rank.equals("J")) {
            rank = "Jack";
        } else if (rank.equals("Q")) {
            rank = "Queen";
        } else if (rank.equals("K")) {
            rank = "King";
        }

        if (suit.equals("D")) {
            suit = "Diamonds";
        } else if (suit.equals("H")) {
            suit = "Hearts";
        } else if (suit.equals("S")) {
            suit = "Spades";
        } else if (suit.equals("C")) {
            suit = "Clubs";
        }

        System.out.println(rank + " of " + suit);


    }
}



