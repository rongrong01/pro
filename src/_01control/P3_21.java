package _01control;

import java.util.Scanner;

/**
 * Created by Haining on 10/2/16.
 */
public class P3_21 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter Income: ");
        double income = input.nextDouble();
        input.close();
        double tax = 0;

        if (income <= 50000) {
            tax = income * 0.01;
        }else if (income <= 75000) {
            tax = income * 0.02;
        }else if (income <= 100000) {
            tax = income * 0.03;
        }else if (income <= 250000) {
            tax = income * 0.04;
        }else if (income <= 500000) {
            tax = income * 0.05;
        }else {
            tax = income * 0.06;
        }

        System.out.println("Income Tax is: " +  tax);


    }
}
