package _01control;

import java.util.Scanner;

/**
 * Created by Haining on 10/2/16.
 */
public class P4_18 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Number: ");
        int number = input.nextInt();
        input.close();

        for(int cur_number =2; cur_number <= number; cur_number++) {
            boolean prime = true;
            int test_number = 2;
            while (prime && test_number < cur_number) {
                if (cur_number % test_number == 0) {
                    prime = false;
                }
                test_number += 1;
            }

                if (prime){
                    System.out.println(cur_number);
            }
        }
    }
}
