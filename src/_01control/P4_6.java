package _01control;

import java.util.Scanner;

/**
 * Created by Haining on 10/2/16.
 */
public class P4_6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Sequence of intergers, seperated by space. When you finish, press 'Enter' followed by Coommand-D: ");
        boolean first = true;
        double min = 0;

        while (input.hasNextDouble()){
            double value = input.nextDouble();
            if (first) {
                min = value;
                first = false;
            } else if (value <  min){
                min = value;
            }
        }
        input.close();
        System.out.println("Min value: " + min);
    }
}
