package _05dice.P11_22;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Haining on 10/30/16.
 */
public class TriangleComponent {
    private static final int RECTANGLE_WIDTH = 2;
    private static final int RECTANGLE_HEIGHT = 2;

    private int xLeft;
    private int yTop;
    ArrayList<Point> dotList = new ArrayList<Point>();

    public TriangleComponent(){
        xLeft = 0;
        yTop = 0;

    }

    public void paintComponent(Graphics g){
        //to make sure the drawings satisfy the problem requirement, and will erase and re-start after every 3 clicks
        int startPrint = dotList.size()-(dotList.size()-1)%3 -1;
        //if there is one click
        if ((dotList.size()-1)%3  == 0){
            g.drawRect((int)dotList.get(startPrint).getX(),(int)dotList.get(startPrint).getY(),
                    RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
        }
        //if there are two clicks
        else if ((dotList.size()-1)%3 == 1){
            g.drawRect((int)dotList.get(startPrint).getX(),(int)dotList.get(startPrint).getY(),
                    RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
            g.drawRect((int)dotList.get(startPrint+1).getX(),(int)dotList.get(startPrint+1).getY(),
                    RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
            g.drawLine((int)dotList.get(startPrint).getX(), (int)dotList.get(startPrint).getY(),
                    (int)dotList.get(startPrint+1).getX(), (int)dotList.get(startPrint+1).getY());

        }
        //if there are three clicks
        else if ((dotList.size()-1)%3 == 2){
            g.drawRect((int)dotList.get(startPrint).getX(),(int)dotList.get(startPrint).getY(),
                    RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
            g.drawRect((int)dotList.get(startPrint+1).getX(),(int)dotList.get(startPrint+1).getY(),
                    RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
            g.drawRect((int)dotList.get(startPrint+2).getX(),(int)dotList.get(startPrint+2).getY(),
                    RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
            g.drawLine((int) dotList.get(startPrint).getX(), (int) dotList.get(startPrint).getY(),
                    (int) dotList.get(startPrint+1).getX(), (int) dotList.get(startPrint+1).getY());
            g.drawLine((int) dotList.get(startPrint+1).getX(), (int) dotList.get(startPrint+1).getY(),
                    (int) dotList.get(startPrint + 2).getX(), (int) dotList.get(startPrint + 2).getY());
            g.drawLine((int)dotList.get(startPrint+2).getX(), (int)dotList.get(startPrint+2).getY(),
                    (int)dotList.get(startPrint).getX(), (int)dotList.get(startPrint).getY());
        }


    }

    public void moveRectangle(ArrayList<Point> points){
        dotList = points;
        //I do not know why I got a compile error here called "cannot find symbol".
        repaint();
    }

}
