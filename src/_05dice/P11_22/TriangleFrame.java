package _05dice.P11_22;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created by Haining on 10/30/16.
 */
public class TriangleFrame extends JFrame{
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 400;

    private ArrayList<Point> pointsAList = new ArrayList<Point>();

    private TriangleComponent scene;

    class MousePressListener implements MouseListener{

        public void mousePressed(MouseEvent event){
            Point onePoint = new Point(event.getX(),event.getY());
            pointsAList.add(onePoint);
            scene.moveRectangle(pointsAList);

        }

        public void mouseClicked(MouseEvent e){};
        public void mouseReleased(MouseEvent e){};
        public void mouseEntered(MouseEvent e){};
        public void mouseExited(MouseEvent e){};
    }

    public TriangleFrame(){
        scene = new TriangleComponent();
        add(scene);

        MouseListener listener = new MousePressListener();
        scene.addMouseListener(listener);

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }
}
