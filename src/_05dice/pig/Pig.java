package _05dice.pig;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * Created by Haining on 10/30/16.
 */
public class Pig {
    private JPanel panel;
    private JTextField textField1;
    private JTextField textField2;
    private JTextArea textArea1;
    private JButton rollAgainButton;
    private JButton startOverButton;
    private JButton endTurnButton;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Order entry");
        frame.setContentPane(new Pig().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(400,400);
        frame.setVisible(true);
    }

    public Pig() {
        ScoreKeeper scoreKeeper = new ScoreKeeper();

        //reset game
        class ResetListener implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                scoreKeeper.newGame();
                textField1.setText("" + (scoreKeeper.getPlayerScore()));
                textArea1.setText("Reset the game." +
                        "\nPlayer's roll.");

                textField2.setText("" + scoreKeeper.getCompScore());
            }
        }

        ActionListener resetButton = new ResetListener();
        startOverButton.addActionListener(resetButton);


        class PlayerListener implements ActionListener {
            int currentRoll;

            public void actionPerformed(ActionEvent e) {
                //If game is in progress and it is the player's turn.
                if (scoreKeeper.isGameInProgress() && scoreKeeper.getPlayerTurn()) {
                    currentRoll = scoreKeeper.getRoll();
                    textArea1.setText("You rolled " + currentRoll + ".\n");
                    //If the current roll equals 1, it is computer's turn.
                    if (currentRoll == 1) {
                        textArea1.append("You rolled a one. \nYour turn is over.\n");
                        scoreKeeper.playerRolledOne();
                        scoreKeeper.setPlayerTurn(false);
                        textField1.setText("" + (scoreKeeper.getPlayerScore()));
                        computerTurn();
                    }

                    //If the current roll is not one.
                    else {
                        //Add the score to the player's score and the current score and print the score.
                        scoreKeeper.addPlayerScore(currentRoll);
                        scoreKeeper.addCurrentScore(currentRoll);
                        textField1.setText("" + (scoreKeeper.getPlayerScore()));
                        //Check to see if the player has won.
                        if (scoreKeeper.getPlayerScore() >= 100) {
                            textArea1.append("You won!");
                            scoreKeeper.setGameInProgress(false);
                        }
                        //
                        else {
                            textArea1.append("Choose button.");
                        }
                    }
                }
            }
            public void computerTurn(){
                int computerRoll = 0;
                while(computerRoll != 1 && scoreKeeper.isGameInProgress()){
                    textArea1.append("Rolling...\n");
                    computerRoll = scoreKeeper.getRoll();
                    System.out.println(computerRoll);
                    textArea1.append("Computer rolled " + computerRoll + ".\n");
                    //If the current roll equals 1, it is player's turn.
                    if (computerRoll == 1){
                        textArea1.append("Player's turn.\n");
                        scoreKeeper.setPlayerTurn(true);
                        scoreKeeper.compRolledOne();
                        textField2.setText(""+ scoreKeeper.getCompScore());
                        scoreKeeper.setPlayerTurn(true);
                    }
                    //If the current roll is not one.
                    else{
                        scoreKeeper.addComputerScore(computerRoll);
                        scoreKeeper.addCurrentScore(computerRoll);
                        textField2.setText("" + scoreKeeper.getCompScore());
                        //Check to see if computer has won.
                        if (scoreKeeper.getCompScore()>100){
                            textArea1.append("\nComputer won!");
                            scoreKeeper.setGameInProgress(false);
                        }
                        //Computer Strategy: if the current score is >20, exit, otherwise continue.
                        else{
                            if (scoreKeeper.getCurrentScore() > 20){
                                scoreKeeper.setPlayerTurn(true);
                                scoreKeeper.setCurrentScore(0);
                                return;
                            }
                        }
                    }
                }
            }
        }
        ActionListener playerB = new PlayerListener();
        rollAgainButton.addActionListener(playerB);

        class EndTurnListener implements ActionListener{

            public void actionPerformed(ActionEvent e) {
                computerTurn();
            }

            //same computerTurn as in PlayerListener.
            //I do not think I should repeat the code here. Should I creat a sepeerate class just for Comupter Turn?
            public void computerTurn(){}

        }

        ActionListener endTurnB= new EndTurnListener();
        endTurnButton.addActionListener(endTurnB);
    }
}
