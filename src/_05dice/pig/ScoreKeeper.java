package _05dice.pig;

/**
 * Created by Haining on 10/30/16.
 */

//Reference: http://www.cs.usfca.edu/~srollins/courses/cs112-s07/web/code/pig/ScoreKeeper.java

public class ScoreKeeper {
    private boolean gameInProgress;
    private boolean playerTurn;
    private int playerScore;
    private int computerScore;
    private int currentScore;
    private final int WINNINGSCORE = 100;
    private int roll;

    public ScoreKeeper(){
        gameInProgress = true;
        int player = 1;
    }

    public boolean isGameInProgress() {
        return gameInProgress;
    }

    public void setGameInProgress(boolean gameInProgress) {
        this.gameInProgress = gameInProgress;
    }

    public boolean getPlayerTurn() {
        return playerTurn;
    }

    public void setPlayer(int player) {
        this.playerTurn = playerTurn;
    }

    public int getPlayerScore() {
        return playerScore;
    }

    public void setPlayerScore(int playerScore) {
        this.playerScore = playerScore;
    }

    public int getCompScore() {
        return computerScore;
    }

    public void setCompScore(int compScore) {
        this.computerScore = compScore;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

    public int getWINNINGSCORE() {
        return WINNINGSCORE;
    }

    public void resetRoundScore(){
        currentScore = 0;
    }

    public void newGame(){
        //Reset the variables to start a new game.
        gameInProgress = true;
        playerTurn = true;
        playerScore = 0;
        computerScore = 0;
        currentScore = 0;

    }

    public int addPlayerScore(int i){
        playerScore += i;
        return playerScore;
    }

    public int addComputerScore(int i){
        computerScore += i;
        return computerScore;
    }

    public int addCurrentScore(int i){
        currentScore += i;
        return currentScore;
    }

    //Generate a random roll.
    public int getRoll(){
        return (int) (Math.random()*6 + 1);
    }

    //Player rolled one.  Reset the current score count and subtract it from the player's total.
    public void playerRolledOne(){
        playerScore -= currentScore;
        currentScore = 0;
    }

    //Computer rolled one.  Reset the current score count and subtract it from the computer's total.
    public void compRolledOne(){
        computerScore -= currentScore;
        currentScore = 0;
    }

    public void setPlayerTurn(boolean t){
        playerTurn = t;
    }
}
