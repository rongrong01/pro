package _05dice.P10_2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Haining on 10/30/16.
 */
public class ButtonFrame1 extends JFrame{

    private JButton button;
    private JLabel label;

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 100;

    public ButtonFrame1()
    {
        createComponents();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);

    }

    class ClickListener implements ActionListener{
        int intClick = 0;
        @Override
        public void actionPerformed(ActionEvent e) {
            intClick++;
            label.setText("I was clicked " + intClick + " times!");
        }
    }

    private void createComponents(){
        button = new JButton("Click me!");
        ActionListener listener = new ClickListener();
        button.addActionListener(listener);


        label = new JLabel("I was clicked 0 times!");

        JPanel panel = new JPanel();
        panel.add(button);
        panel.add(label);
        add(panel);

    }

}
