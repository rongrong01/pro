package _05dice.P10_2;

import javax.swing.*;

/**
 * Created by Haining on 10/30/16.
 */
public class ButtonViewer1 {
    public static void main(String[] args) {
        //Create a new ButtonFrame1 objects, make it Exit on Close and make it visible.
        JFrame frame = new ButtonFrame1();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
