package _05dice.P10_26;


import javax.swing.*;
import java.awt.*;

/**
 * Created by Haining on 10/30/16.
 */
public class RingComponent extends JComponent{
    private int x;
    private int y;
    private int diameter;
    private Color color;

    public RingComponent(int x, int y, int diameter, Color color){
        this.x = x;
        this.y = y;
        this.diameter = diameter;
        this.color = color;
    }

    public void paintComponent(Graphics g){
        g.setColor(Color.YELLOW);
        g.drawOval(1,0,40,40);
        g.setColor(Color.BLACK);
        g.drawOval(21,20,40,40);
        g.setColor(Color.GREEN);
        g.drawOval(41,0,40,40);
        g.setColor(Color.RED);
        g.drawOval(61,20,40,40);
        g.setColor(Color.BLUE);
        g.drawOval(81,0,40,40);

    }
}

