package _05dice.P10_26;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Haining on 10/30/16.
 */
public class RingViewer {
    public static void main(String[] args) {

        JFrame frame = new JFrame();

        frame.setSize(300, 200);
        frame.setTitle("Olympic Rings");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        RingComponent ringComp = new RingComponent(1, 1, 1, Color.WHITE);
        frame.add(ringComp);

        frame.setVisible(true);
    }
}
