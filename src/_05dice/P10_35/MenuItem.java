package _05dice.P10_35;

/**
 * Created by Haining on 10/30/16.
 */
public class MenuItem {
    private String name;
    private double price;
    private int count = 0;


    public MenuItem(String name, double price){
        this.name = name;
        this.price = price;

    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
