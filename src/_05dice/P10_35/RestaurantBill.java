package _05dice.P10_35;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * Created by Haining on 10/30/16.
 */
public class RestaurantBill {
    private JButton cokeButton;
    private JButton spriteButton;
    private JButton lemonadeButton;
    private JButton hotTeaButton;
    private JButton icedTeaButton;
    private JButton coldBrewCoffeeButton;
    private JButton icedCoffeeButton;
    private JButton latteButton;
    private JButton expressoButton;
    private JButton hotChocolateButton;
    private JPanel panel;
    private JTextField textField1;
    private JTextField textField2;
    private JTextArea textArea1;
    private JButton calculateBillButton;
    private JButton addItemButton;


    public static void main(String[] args) {
        JFrame frame = new JFrame("Order");
        frame.setContentPane(new RestaurantBill().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(800, 600);
        frame.setVisible(true);
    }

    public RestaurantBill(){
        final ArrayList<MenuItem> menuBill = addItems();

        class MenuListener implements ActionListener{
            int choice;
            public MenuListener(int buttonNumer){
                choice = buttonNumer;
            }
            @Override
            public void actionPerformed(ActionEvent e) {
                menuBill.get(choice).setCount(menuBill.get(choice).getCount() + 1);
            }
        }

        ActionListener cokeListener = new MenuListener(0);
        cokeButton.addActionListener(cokeListener);

        ActionListener spriteListener = new MenuListener(1);
        spriteButton.addActionListener(spriteListener);

        ActionListener lemonadeListener = new MenuListener(2);
        lemonadeButton.addActionListener(lemonadeListener);

        ActionListener hotTeaButtonListener = new MenuListener(3);
        hotTeaButton.addActionListener(hotTeaButtonListener);

        ActionListener icedTeaButtonListener = new MenuListener(4);
        icedTeaButton.addActionListener(icedTeaButtonListener);

        ActionListener coldBrewCoffeeListener = new MenuListener(5);
        coldBrewCoffeeButton.addActionListener(coldBrewCoffeeListener);

        ActionListener icedCoffeeListener = new MenuListener(6);
        icedCoffeeButton.addActionListener(icedCoffeeListener);

        ActionListener latteListener = new MenuListener(7);
        latteButton.addActionListener(latteListener);

        ActionListener expressoListener = new MenuListener(8);
        expressoButton.addActionListener(expressoListener);

        ActionListener hotChocolateListener = new MenuListener(9);
        hotChocolateButton.addActionListener(hotChocolateListener);

        //An inner class to read in the two text fields when the button AddItem is clicked. It is for the less popular items.
        class AddItemListener implements ActionListener{
            public void actionPerformed(ActionEvent e) {
                String strNewItem = textField1.getText();
                double newPrice = Double.parseDouble(textField2.getText());

                MenuItem newItem = new MenuItem(strNewItem, newPrice);
                newItem.setCount(1);
                menuBill.add(newItem);

                textField1.setText("");
                textField2.setText("");
            }
        }

        ActionListener addItemListener = new AddItemListener();
        addItemButton.addActionListener(addItemListener);

        //An inner class to calculate the total bill, including tax (10.25% of bill) suggested tips (15% of bill).
        class BillListener implements ActionListener{
            double totalBill = 0;
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea1.append("\n************************************************\n");
                for (int i = 0; i < menuBill.size(); i++) {
                    if(menuBill.get(i).getCount()>0){
                        textArea1.append(menuBill.get(i).getName() + " x" + menuBill.get(i).getCount() + " $" +
                                menuBill.get(i).getCount()* menuBill.get(i).getPrice()+ "\n");
                        totalBill += menuBill.get(i).getCount()* menuBill.get(i).getPrice();
                    }
                }
                textArea1.append("Food and drink total is: $" + String.format("%.2f", totalBill));
                textArea1.append("\nTax: $" + String.format("%.2f", totalBill*.1025));
                textArea1.append("\nTip: $" + String.format("%.2f", totalBill*.15));
                textArea1.append("\nYou owe: $" + String.format("%.2f", totalBill*1.25));
            }
        }
        ActionListener billButtonL = new BillListener();
        calculateBillButton.addActionListener(billButtonL);
    }

    public static ArrayList<MenuItem> addItems(){
        ArrayList<MenuItem> menuBill = new ArrayList<MenuItem>();
        MenuItem coke = new MenuItem("Coke" , 2.25);
        MenuItem sprite = new MenuItem("Sprite" , 2.25);
        MenuItem lemonade = new MenuItem("Lemonade" , 2.50);
        MenuItem hotTea = new MenuItem("Hot Tea" , 3.00);
        MenuItem icedTea = new MenuItem("Iced Tea" , 3.00);
        MenuItem coldBrewCoffee = new MenuItem("Cold Brew Coffee" , 4.00);
        MenuItem icedCoffee = new MenuItem("Iced Coffee" , 3.50);
        MenuItem latte = new MenuItem("Latte" , 4.50);
        MenuItem expresso = new MenuItem("Expresso" , 2.75);
        MenuItem hotChocolate= new MenuItem("Hot Chocolate" , 3.75);
        menuBill.add(coke);
        menuBill.add(sprite);
        menuBill.add(lemonade);
        menuBill.add(hotTea);
        menuBill.add(icedTea);
        menuBill.add(coldBrewCoffee);
        menuBill.add(icedCoffee);
        menuBill.add(latte);
        menuBill.add(expresso);
        menuBill.add(hotChocolate);
        return menuBill;
    }

}
